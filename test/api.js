//Se instancian los paquetes de test
var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
var server = require('../server')
//chai es el motor de reglas y le diremos que use las de http
chai.use(chaiHttp) //Configurar Chai en el modulo de HTTP

//Verificar que tengo conectividad con internet
//Se deben definir Conjuntos de Pruebas con la palabra clave describe
// () =>  significa.. Dado un valor quiero que me proporciones...
describe('Tets de conectividad', () =>{
    it('Google function', (done) => {
      //Cómo pruebo que Google funciona?
      chai.request('http://www.google.com.mx').get('/').end((err,res) =>{
        //En este caso se espera un valor
        //console.log(res)
        res.should.have.status(200)
        //Terminamos la prueba
        done()
      })
    })
})

describe('Tets de API usuarios', () =>{
  it('Raiz de la API contesta', (done) => {
    //Cómo pruebo que Google funciona?
    chai.request('http://localhost:3000').get('/v3').end((err,res) =>{
      //En este caso se espera un valor
      //console.log(res)
      res.should.have.status(200)
      //Terminamos la prueba
      done()
    })
  })

  it('Raiz de la API Funciona', (done) => {
    //Cómo pruebo que Google funciona?
    chai.request('http://localhost:3000').get('/v3').end((err,res) =>{
      //En este caso se espera un valor
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      //Terminamos la prueba
      done()
    })
  })

  it('Raiz de la API devuelve 2 colecciones', (done) => {
    //Cómo pruebo que Google funciona?
    chai.request('http://localhost:3000').get('/v3').end((err,res) =>{
      //En este caso se espera un valor
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      //Terminamos la prueba
      done()
    })
  })

  it('Raiz de la API devuelve los objetos correctos', (done) => {
    //Cómo pruebo que Google funciona?
    chai.request('http://localhost:3000').get('/v3').end((err,res) =>{
      //En este caso se espera un valor
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso')
        res.body[i].should.have.property('url')
      }
      //Terminamos la prueba
      done()
    })
  })
})



describe('Tets de API movimientos', () =>{
  it('Raiz de la API Movimientos contesta', (done) => {
    chai.request('http://localhost:3000').get('/V1/movimientos').end((err,res) =>{
      //console.log(res)
      res.should.have.status(200)
      //Terminamos la prueba
      done()
    })
  })
})
