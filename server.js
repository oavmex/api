console.log("Aqui estoy funcionando");

var movimientosJSON = require('./movimientosV2.json');
var usuariosJSON = require('./usuariosV1.json');  //se agrega archivo USUARIOS.JSON

var express = require('express');
var bodyparser = require('body-parser');  //Para leer el contenido del BODY

var jsonQuery=require('json-query');
var requestJson=require('request-json');

var app     = express(); //se instancia

app.use(bodyparser.json());

//Se enviarà un mensaje al Navegador
app.get('/',function(req,res)
{
    res.send('Hola API');
})

app.get('/V1/movimientos',function(req,res)
{
    res.sendfile('movimientosV1.json');
})

app.get('/V2/movimientos',function(req,res)
{
  //res.send('mov1,mov2,mov3');
//    res.sendfile('movimientosV2.json');
  res.send(movimientosJSON);
});
//Pasarle parametros a la url
app.get('/v2/movimientos/:id',function(req,res) {
    console.log(req.params.id);
        console.log(movimientosJSON);
    //res.send('Ha solicitado consultar el movimiento: '+req.params.id);
    //sendfile is deprecated
    //res.sendfile('movimientosv2.json');
    res.send(movimientosJSON[req.params.id-1]);
});

// -------
//usuarios  ID
app.get('/v1/usuarios',function(req,res)
{
  res.send(usuariosJSON);
});
app.get('/v1/usuarios/:id',function(req,res) {
    console.log(req.params.id);
        console.log(usuariosJSON);
    res.send(usuariosJSON[req.params.id-1]);
});

//usuarios  LOGIN

app.post('/v1/usuarios/login', function (req,res){
  var email = req.headers['email'];
  var pass = req.headers['pass'];
  var resultados = jsonQuery ('[email= '+ email + ']', {data:usuariosJSON})
  if (resultados.value != null && resultados.value.pass == pass){
    usuariosJSON[resultados.value.id - 1].estado=true
    res.send('{"login":"OK"}')
  }
  else {
    res.send('{"login":"error"}')
  }
});

//usuarios  LOGOUT
app.post('/v1/usuarios/logout/:id', function (req,res){
  var id = req.params.id ;
  var usuario = usuariosJSON[id-1];

  if (usuario.estado==true){
    usuario.estado=false
    res.send('{"logout":"OK"}')
  }
  else {
    res.send('{"logout":"error"}')
  }
});


app.get('/v1/usuarios/:id',function(req,res) {
    console.log(req.params.id);
    res.send(usuariosJSON[req.params.id-1]);
});
// -------



app.get('/v2/movimientosq',function(req,res) {
    console.log(req.query);
    res.send ("Recibido");
});

app.get('/v2/movimientosp',function(req,res) {
    console.log(req.params);
    res.send ("Recibido");
});


  app.post ('/v2/movimientos', function (req,res){
    console.log (req);
    console.log (req.headers['authorization']);
    if(req.headers ['authorization'] != undefined)
    {
      var nuevo = req.body;
      nuevo.id = movimientosJSON.length + 1;
      movimientosJSON.push(nuevo);
      res.send ("Movimiento dado de alta");
    }
    else {
      res.send ("No esta autorizado");
    }
  });

//Modificar un movimiento
/*
  app.put('/V2/movimientos/:id',function(req,res)
  {
      var cambios = req.body
      movimientosJSON[ req.params.id=1 ] = cambios
      res.send("Mov modificado")
  })
*/
  app.put('/V2/movimientos/:id',function(req,res)
  {
      var cambios = req.body;
      var actual  = movimientosJSON[ req.params.id-1 ]
      if (cambios.importe != undefined){
        actual.importe = cambios.importe;
      }
      if (cambios.ciudad != undefined){
        actual.ciudad = cambios.ciudad;
      }

      res.send("Mov modificado");
  });


app.delete('/V2/movimientos/:id',function(req,res){
  var actual = movimientosJSON[ req.params.id - 1 ];
  movimientosJSON.push({
    "id" : movimientosJSON.lenght + 1,
    "ciudad" : actual.ciudad,
    "importe" : actual.importe * (-1),
    "ip_address" : "Negativo del " + req.params.id
  });
  res.send("Movimiento anulado");
})

var urlMlabRaiz = 'https://api.mlab.com/api/1/databases/techu-mx-oav/collections';
var apiKey = "apiKey=OGFoUS0EOk6OqYLmzGd2rHhVlnk7ZV-z"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)
//esto continuá siendo EXPRESS, cuando alguien ponga /V3
app.get('/V3',function(req,res){
    //Se requiere hacer la llamada de la API MLAB a nivel BD y con la API
    //Se utiliza RequestJson y las var Raiz + ? + Key para traer las colecciones
    //Función que trae si hay error, la respuesta y el body en formato Json
    clienteMlab.get('', function(err, resM, body) {
      var coleccionesUsuario = []
      if (!err){
        for (var i = 0; i < body.length; i++) {
          if (body[i] != "system.indexes"){
            coleccionesUsuario.push({"recurso":body[i],"url":"/v3/" + body[i]})
          }
        }
        res.send(coleccionesUsuario);
      }
      else {
        res.send(err);
      }
    })
})

//Se utiliza RequestJson y las var Raiz + "/usuarios?"  + Key para traer los usuarios
app.get('/V3/usuarios',function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get('', function (err,resM,body) {
    res.send(body)
  })
});
//Par dar de alta un usuario
app.post('/V3/usuarios', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    //req.body  ya es lo que me manda POrtman y es un Json
    clienteMlab.post('',req.body,function (err,resM,body) {
    //res.send(body)
    if (err)
         res.send('Error');
      else
        res.send('Success');
  })
});

//Consultr un ID de usuario específico
//Se utiliza
//"q" example - return all documents with "active" field of true:
// https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&apiKey=myAPIKey
// https://api.mlab.com/api/1/databases/techu-mx-oav/collections/usuarios?apiKey=OGFoUS0EOk6OqYLmzGd2rHhVlnk7ZV-z&q={"nombre": "Barby"}
// https://api.mlab.com/api/1/databases/techu-mx-oav/collections/usuarios?apiKey=OGFoUS0EOk6OqYLmzGd2rHhVlnk7ZV-z&q={"id":1}
//+ apiKey deb estar al último de la petición, en la URL de arriba va enmedio y aplicaría en dónde sea pero no con MLAB
app.get('/V3/usuarios/:id',function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
    clienteMlab.get('?&q={"id":' +req.params.id + '}&'+ apiKey, function (err,resM,body) {
    res.send(body)
  })
});

//update or delete a single document
//http://docs.mlab.com/data-api/
app.put('/V3/usuarios/:id',function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?&q={"id":' +req.params.id + '}&' + apiKey)
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
  console.log(cambio)
    clienteMlab.put('',JSON.parse(cambio), function (err,resM,body) {
    res.send(body)
  })
});

//----------------------------------------
  app.listen(3000);  //se asigna un puerto
  console.log("Escuchando el puerto 3000");
